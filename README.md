## Problem analysis

Given an undirected graph it was needed to find non-overlapping communities maximizing modularity + regularization score.

Full problem statement is available on https://codeforces.com/contest/1378/

It is a wellknown algorithmic problem, one of the most popular algorithm for solving it is Louvain algorithm (https://arxiv.org/abs/0803.0476)

Also there are some improvements of this algorithm (ex. https://arxiv.org/pdf/1503.01322.pdf, https://www.researchgate.net/publication/301929529_Improving_the_Louvain_Algorithm_for_Community_Detection_with_Modularity_Maximization)
But those algorithms do not using regularization term for scoring community detection.

## Current approach

This project based on open-source project described here (https://www.cwts.nl/blog?article=n-r2u2a4).
It contains already Leiden and Louvain algorithms for solving communities detection problem.
Source code is publicly available on github and released under MIT license (https://github.com/CWTSLeiden/networkanalysis)

##### Actual implemented approach:
- use Leiden/Louvain/RandomizedLouvain algorithm for initial network clustering
- improve regularization score separating some nodes into a singleton clusters
- run this algorithm several times with different random state and choose the best clustering

## Build and run

Project uses standard gradle build.

- `./gradlew run --args="--input {input_graph} --output {output_clustering}`
- `./gradlew installDist && ./core/build/install/core/bin/core --input {input_graph} --output {output_clustering}`

## Test results

For given three input graphs

|         | N_Clusters | Modularity         | Regularization      | Sum                | Total        |
|---------|------------|--------------------|---------------------|--------------------|--------------|
| 1.graph | 3197       | 0.7106632447252577 | 0.47638949263461655 | 1.1870527373598743 |              |
| 2.graph | 1648       | 0.5183587681807892 | 0.4741902555706031  | 0.9925490237513923 | 3.0720651404 |
| 3.graph | 597        | 0.4008982498400022 | 0.491565129412663   | 0.8924633792526653 |              |

## Author
- Sergey Kiyan (sokian92@gmail.com)

## License

This project is distributed under the MIT License.
Please refer to the [`LICENSE`](LICENSE) file for further details.
