repositories {
    jcenter()
    mavenCentral()
}

allprojects {
    group = "com.sokian.huawei"

    apply {
        plugin("idea")
        plugin("java")
        plugin("scala")
        plugin("java-library")
    }

    repositories {
        mavenCentral()
        jcenter()
    }
}
