plugins {
    application
}

dependencies {
    implementation(project(":networkanalysis"))

    implementation("org.slf4j:slf4j-api:1.7.30")
    implementation("org.slf4j:slf4j-simple:1.7.30")

    implementation("it.unimi.dsi:dsiutils:2.6.7") {
        exclude("ch.qos.logback", "logback-classic")
    }

    // Use Scala 2.13 in our library project
    implementation("org.scala-lang:scala-library:2.13.2")
    implementation("com.github.scopt:scopt_2.13:3.7.1")

    // Use Scalatest for testing our library
    testImplementation("junit:junit:null")
    testImplementation("org.scalatest:scalatest_2.13:3.1.2")
    testImplementation("org.scalatestplus:junit-4-12_2.13:3.1.2.0")

    // Need scala-xml at test runtime
    testRuntimeOnly("org.scala-lang.modules:scala-xml_2.13:1.2.0")
}

application {
    mainClass.set("com.sokian.graph.community.Main")
}
