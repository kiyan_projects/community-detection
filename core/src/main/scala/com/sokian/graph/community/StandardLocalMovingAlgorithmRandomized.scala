package com.sokian.graph.community

import java.util.Random

import cwts.networkanalysis.{Clustering, IncrementalCPMClusteringAlgorithm, Network}
import cwts.util.Arrays
import it.unimi.dsi.util.XoRoShiRo128PlusRandom

import scala.collection.mutable.ArrayBuffer

class StandardLocalMovingAlgorithmRandomized(random: Random) extends IncrementalCPMClusteringAlgorithm {
  override def improveClustering(network: Network, clustering: Clustering): Boolean = {
    if (network.getNNodes == 1) {
      return false
    }
    val defaultMaxIterations = 30
    val baseEdgesSize = 8000000.0
    var maxIterations = Math.round(defaultMaxIterations / (network.getNEdges / baseEdgesSize)).toInt
    maxIterations = Math.min(Math.max(maxIterations, 1), defaultMaxIterations)

    val fastRandom = new XoRoShiRo128PlusRandom(random.nextLong())

    var update = false

    val clusterWeights = new Array[Double](network.getNNodes)
    val nNodesPerCluster = new Array[Int](network.getNNodes)

    for (i <- 0 until network.getNNodes) {
      clusterWeights(clustering.getCluster(i)) += network.getNodeWeight(i)
      nNodesPerCluster(clustering.getCluster(i)) += 1
    }

    var nUnusedClusters = 0
    val unusedClusters = new Array[Int](network.getNNodes - 1)
    for (i <- network.getNNodes - 1 to 0 by -1) {
      if (nNodesPerCluster(i) == 0) {
        unusedClusters(nUnusedClusters) = i
        nUnusedClusters += 1
      }
    }

    var nodeOrder = Arrays.generateRandomPermutation(network.getNNodes, random)

    /*
     * Iterate over the nodeOrder array in a cyclical manner. When the end
     * of the array has been reached, start again from the beginning.
     * Continue iterating until none of the last nNodes node visits has
     * resulted in a node movement.
     */
    val edgeWeightPerCluster = new Array[Double](network.getNNodes)
    val neighboringClusters = new Array[Int](network.getNNodes)

    var nUnstableNodes = network.getNNodes

    def percentileIncrease(value: Double, percentile: Double): Double = {
      Math.min(value * (1.0 - percentile), value * (1.0 + percentile))
    }

    var i = 0
    var iteration = 0
    do {
      val node = nodeOrder(i)
      val currentCluster = clustering.getCluster(node)

      // Remove the currently selected node from its current cluster.
      clusterWeights(currentCluster) -= network.getNodeWeight(node)
      nNodesPerCluster(currentCluster) -= 1
      if (nNodesPerCluster(currentCluster) == 0) {
        unusedClusters(nUnusedClusters) = currentCluster
        nUnusedClusters += 1
      }

      /*
       * Identify the neighboring clusters of the currently selected
       * node, that is, the clusters with which the currently selected
       * node is connected. An empty cluster is also included in the set
       * of neighboring clusters. In this way, it is always possible that
       * the currently selected node will be moved to an empty cluster.
       */
      neighboringClusters(0) = unusedClusters(nUnusedClusters - 1)
      var nNeighboringClusters = 1
      val neighbors = network.getNeighbors(node)
      val edgeWeights = network.getEdgeWeights(node)
      for (k <- neighbors.indices) {
        val neighbor = neighbors(k)
        val edgeWeight = edgeWeights(k)
        val neighborCluster = clustering.getCluster(neighbor)
        if (edgeWeightPerCluster(neighborCluster) == 0) {
          neighboringClusters(nNeighboringClusters) = neighborCluster
          nNeighboringClusters += 1
        }
        edgeWeightPerCluster(neighborCluster) += edgeWeight
      }

      /*
       * For each neighboring cluster of the currently selected node,
       * calculate the increment of the quality function obtained by
       * moving the currently selected node to the neighboring cluster.
       * Determine the neighboring cluster for which the increment of the
       * quality function is largest. The currently selected node will be
       * moved to this optimal cluster. In order to guarantee convergence
       * of the algorithm, if the old cluster of the currently selected
       * node is optimal but there are also other optimal clusters, the
       * currently selected node will be moved back to its old cluster.
       */
      var bestCluster = currentCluster
      val initialQualityValueIncrement = edgeWeightPerCluster(currentCluster) - network.getNodeWeight(node) * clusterWeights(currentCluster) * resolution

      val candidates = new ArrayBuffer[(Double, Int)]()
      candidates.append((-initialQualityValueIncrement, currentCluster))

      for (k <- 0 until nNeighboringClusters) {
        val neighborCluster = neighboringClusters(k)
        val qualityValueIncrement = edgeWeightPerCluster(neighborCluster) - network.getNodeWeight(node) * clusterWeights(neighborCluster) * resolution
        if (qualityValueIncrement > initialQualityValueIncrement) {
          candidates.append((-qualityValueIncrement, neighborCluster))
        }
        edgeWeightPerCluster(neighborCluster) = 0
      }
      candidates.sortInPlace()

      var loopDone = false
      for (candidate <- candidates) {
        if (!loopDone && fastRandom.nextDouble() < 0.95) {
          bestCluster = candidate._2
          loopDone = true
        }
      }

      /*
       * Move the currently selected node to its new cluster. Update the
       * clustering statistics.
       */
      clusterWeights(bestCluster) += network.getNodeWeight(node)
      nNodesPerCluster(bestCluster) += 1
      if (bestCluster == unusedClusters(nUnusedClusters - 1)) {
        nUnusedClusters -= 1
      }
      nUnstableNodes -= 1
      /*
       * If the new cluster of the currently selected node is different
       * from the old cluster, some further updating of the clustering
       * statistics is performed.
       */
      if (bestCluster != currentCluster) {
        clustering.setCluster(node, bestCluster)
        nUnstableNodes = network.getNNodes
        update = true
      }
      i = if (i < network.getNNodes - 1) {
        i + 1
      } else {
        nodeOrder = Arrays.generateRandomPermutation(network.getNNodes, random)
        iteration += 1
        0
      }
    } while (nUnstableNodes > 0 && iteration < maxIterations)

    if (update) clustering.removeEmptyClusters()

    update
  }
}
