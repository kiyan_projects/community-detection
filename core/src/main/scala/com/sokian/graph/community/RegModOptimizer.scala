package com.sokian.graph.community

import scala.util.Random

import com.sokian.graph.community.RegModOptimizer.calcDensity
import cwts.networkanalysis.{Clustering, Network}
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable.ArrayBuffer

class RegModOptimizer(val network: Network, val clustering: Clustering) {
  implicit private val logger: Logger = LoggerFactory.getLogger(classOf[RegModOptimizer])
  private val EPS = 1e-4

  val clusters: Array[Int] = clustering.getClusters

  val clusterSizes: ArrayBuffer[Long] = new ArrayBuffer[Long]()
  val clusterEdges: ArrayBuffer[Long] = new ArrayBuffer[Long]()
  val clusterDegrees: ArrayBuffer[Long] = new ArrayBuffer[Long]()

  {
    val (clusterSizesTmp, clusterEdgesTmp, clusterDegreesTmp) = ModularityRegularizationOptimization.calcClusterSizesEdgesDegrees(network, clustering)
    clusterSizes.addAll(clusterSizesTmp)
    clusterEdges.addAll(clusterEdgesTmp)
    clusterDegrees.addAll(clusterDegreesTmp)
  }

  val nodeDegrees: Array[Long] = new Array[Long](network.getNNodes)
  var sumDensity = 0.0
  var nClusters: Int = clustering.getNClusters

  for (i <- 0 until network.getNNodes) {
    nodeDegrees(i) += network.getNNeighbors(i)
  }

  for (i <- 0 until nClusters) {
    sumDensity += calcDensity(clusterEdges(i), clusterSizes(i))
  }

  private def getModularityForCluster(ce: Long, cd: Long): Double = {
    val nEdges = network.getNEdges.toDouble
    val w = cd / 2.0 / nEdges
    ce / nEdges - w * w
  }

  private def getRegularizationScore(nClusters: Long, sumDensity: Double): Double = {
    var regularization = sumDensity / nClusters
    regularization -= nClusters / network.getNNodes.toDouble
    regularization *= 0.5

    regularization
  }

  def getDeltaScore(node: Int): Double = {
    val cluster = clusters(node)
    if (clusterSizes(cluster) == 1) {
      return 0.0
    }
    val currentScore = getModularityForCluster(clusterEdges(cluster), clusterDegrees(cluster)) +
      getRegularizationScore(nClusters, sumDensity)

    var newClusterEdges = clusterEdges(cluster)
    for (j <- network.getNeighbors(node)) {
      val cluster2 = clusters(j)
      if (cluster2 == cluster) {
        newClusterEdges -= 1
      }
    }
    val newClusterDegree = clusterDegrees(cluster) - network.getNNeighbors(node)
    val newSumDensity = sumDensity -
      calcDensity(clusterEdges(cluster), clusterSizes(cluster)) +
      calcDensity(newClusterEdges, clusterSizes(cluster) - 1) +
      calcDensity(0, 1)

    val newScore = getModularityForCluster(newClusterEdges, newClusterDegree) +
      getModularityForCluster(0, network.getNNeighbors(node)) +
      getRegularizationScore(nClusters + 1, newSumDensity)

    newScore - currentScore
  }

  def moveToNewCluster(node: Int): Unit = {
    val cluster = clusters(node)
    if (clusterSizes(cluster) == 1) {
      throw new IllegalArgumentException("Cluster size should be greater than zero for moving")
    }
    clusters(node) = nClusters

    sumDensity -= calcDensity(clusterEdges(cluster), clusterSizes(cluster))

    for (j <- network.getNeighbors(node)) {
      val cluster2 = clusters(j)
      if (cluster2 == cluster) {
        clusterEdges(cluster) -= 1
      }
    }
    clusterSizes(cluster) -= 1
    clusterDegrees(cluster) -= network.getNNeighbors(node)
    clusterSizes.append(1)
    clusterEdges.append(0)
    clusterDegrees.append(network.getNNeighbors(node))

    sumDensity += calcDensity(clusterEdges(cluster), clusterSizes(cluster))
    sumDensity += calcDensity(clusterEdges(nClusters), clusterSizes(nClusters))

    nClusters += 1
  }

  def greedyImproveClustering(): Clustering = {
    var updated = true
    var it = 0
    while (updated) {
      it += 1
      var bestDeltaScore = 0.0
      var bestNode = -1
      for (node <- 0 until network.getNNodes) {
        val deltaScore = getDeltaScore(node)
        if (deltaScore > bestDeltaScore) {
          bestDeltaScore = deltaScore
          bestNode = node
        }
      }
      if (bestDeltaScore > EPS) {
        updated = true
        moveToNewCluster(bestNode)
      } else {
        updated = false
      }
      if (it % 100 == 0) {
        logger.debug("Greedy made " + it + " iterations, current score " + ModularityRegularizationOptimization.calcQuality(network, new Clustering(clusters)))
      }
    }
    logger.info("Greedy improve made " + it + " iterations, score " + ModularityRegularizationOptimization.calcQuality(network, new Clustering(clusters)))
    new Clustering(clusters)
  }

  def randomOrderImprove(): Clustering = {
    var updated = true
    var it = 0
    while (updated) {
      it += 1
      updated = false
      var eps = 1e-2
      for (innerIt <- 0 until 4) {
        Random.setSeed(it * 100 + innerIt)
        val nodes = Random.shuffle(Array.range(0, network.getNNodes))
        for (node <- nodes) {
          val deltaScore = getDeltaScore(node)
          if (deltaScore > eps) {
            moveToNewCluster(node)
            updated = true
          }
        }
        eps /= 10.0
      }
      if (it % 100 == 0) {
        logger.debug("Random order improve made " + it + " iterations, current score " + ModularityRegularizationOptimization.calcQuality(network, new Clustering(clusters)))
      }
    }
    logger.info("Random order improve made " + it + " iterations, score " + ModularityRegularizationOptimization.calcQuality(network, new Clustering(clusters)))
    new Clustering(clusters)
  }
}

object RegModOptimizer {
  def calcDensity(ce: Long, cs: Long): Double = {
    if (cs <= 1) {
      1.0
    } else {
      val maxEdges = cs * (cs - 1) / 2
      ce / maxEdges.toDouble
    }
  }
}
