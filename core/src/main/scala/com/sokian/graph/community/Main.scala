package com.sokian.graph.community

import java.io.{BufferedWriter, File, FileOutputStream, OutputStreamWriter}

import cwts.networkanalysis._
import org.slf4j.{Logger, LoggerFactory}

import scala.collection.mutable.ArrayBuffer

object Main {
  implicit private val logger: Logger = LoggerFactory.getLogger(Main.getClass)

  case class Config
  (
    inputGraph: File = new File("input.txt"),
    output: File = new File("output.txt")
  )

  def main(args: Array[String]): Unit = {
    val parser = new scopt.OptionParser[Config]("gcf") {
      head("Graph communities finder", "0.0.1")

      opt[File]('i', "input")
        .required()
        .valueName("<file>")
        .action((x, c) => c.copy(inputGraph = x))
        .text("input graph file")

      opt[File]('o', "output")
        .required()
        .valueName("<file>")
        .action((x, c) => c.copy(output = x))
        .text("out communities file")

      help("help").text("Graph communities finder based on louvain algorithm")
    }

    parser.parse(args, Config()) match {
      case Some(config) =>
        runCommunitiesFinder(config)
      case None =>
    }
  }

  def runCommunitiesFinder(config: Config): Unit = {
    var network: Network = null
    try {
      network = GraphUtils.readEdgeList(config.inputGraph, useModularity = true, weightedEdges = false, sortedEdgeList = false)
    } catch {
      case e: Exception =>
        logger.error("Error while reading edge list from file", e)
        System.exit(-1)
    }

    logger.info("Network consists of " + network.getNNodes + " nodes and " + network.getNEdges + " edges.")
    val startTimeAlgorithm = System.currentTimeMillis

    val resolution = 1.0
    val initialSeed = 123321 // Lucky magic number. Ha-ha
    val nRandomStarts = 10

    val nIterations = 50

    val initialClustering: Clustering = new Clustering(network.getNNodes)
    val resolution2 = resolution / (2 * network.getTotalEdgeWeight + network.getTotalEdgeWeightSelfLinks)
    var finalClustering: Clustering = null
    var maxQuality = Double.NegativeInfinity

    def runWithAlgorithm(iteration: Int, message: String, algorithm: IterativeCPMClusteringAlgorithm): Unit = {
      var clustering = initialClustering.clone
      algorithm.improveClustering(network, clustering)
      logger.info("Base algorithm score " + algorithm.calcQuality(network, clustering))

      {
        val modularity = ModularityRegularizationOptimization.calcModularity(network, clustering)
        val regularization = ModularityRegularizationOptimization.calcRegularization(network, clustering)
        val quality = modularity + regularization

        logger.info("Random start " + message + " " + iteration + " after base modularity equals " + modularity + ".")
        logger.info("Random start " + message + " " + iteration + " after base regularization equals " + regularization + ".")
        logger.info("Random start " + message + " " + iteration + " after base cluster size equals " + clustering.getNClusters + ".")
        logger.info("Random start " + message + " " + iteration + " after base score equals " + quality + ".")
      }

      val regModOptimizer = new RegModOptimizer(network, clustering)
      clustering = regModOptimizer.randomOrderImprove()

      {
        val modularity = ModularityRegularizationOptimization.calcModularity(network, clustering)
        val regularization = ModularityRegularizationOptimization.calcRegularization(network, clustering)
        val quality = modularity + regularization

        logger.info("Random start " + message + " " + iteration + " after regularization improve modularity equals " + modularity + ".")
        logger.info("Random start " + message + " " + iteration + " after regularization improve regularization equals " + regularization + ".")
        logger.info("Random start " + message + " " + iteration + " after regularization improve cluster size equals " + clustering.getNClusters + ".")
        logger.info("Random start " + message + " " + iteration + " after regularization improve score equals " + quality + ".")
      }

      val quality = ModularityRegularizationOptimization.calcQuality(network, clustering)
      if (quality > maxQuality) {
        finalClustering = clustering
        maxQuality = quality
      }
      logger.info("Random start " + message + " " + iteration + " best score equals " + maxQuality + ".")
    }

    for (i <- 0 until nRandomStarts) {
      runWithAlgorithm(i + 1, "randomizedLouvain", new LouvainAlgorithm(resolution2, nIterations, new StandardLocalMovingAlgorithmRandomized(new java.util.Random(initialSeed + i))))
      writeClustering(config.output, finalClustering)
    }

    for (i <- 0 until nRandomStarts) {
      runWithAlgorithm(i + 1, "leiden", new LeidenAlgorithm(resolution2, nIterations, 5e-2, new java.util.Random(initialSeed + i)))
      writeClustering(config.output, finalClustering)
    }

    for (i <- 0 until nRandomStarts) {
      runWithAlgorithm(i + 1, "louvain", new LouvainAlgorithm(resolution2, -1, new java.util.Random(initialSeed + i)))
      writeClustering(config.output, finalClustering)
    }

    logger.info("Running algorithm took " + (System.currentTimeMillis - startTimeAlgorithm) / 1000 + "s.")

    val modularity = ModularityRegularizationOptimization.calcModularity(network, finalClustering)
    val regularization = ModularityRegularizationOptimization.calcRegularization(network, finalClustering)
    val quality = modularity + regularization

    logger.info("Clusters " + finalClustering.getNClusters + ".")
    logger.info("Modularity equals " + modularity + ".")
    logger.info("Regularization equals " + regularization + ".")
    logger.info("Score equals " + quality + ".")
  }

  private def writeClustering(output: File, clustering: Clustering): Unit = {
    try {
      writeClusteringImpl(output, clustering)
    } catch {
      case e: Exception =>
        logger.error("Error while writing final clustering to file", e)
        System.exit(-1)
    }
  }

  private def writeClusteringImpl(output: File, clustering: Clustering): Unit = {
    output.getParentFile.mkdirs()
    val clusters = Array.fill(clustering.getNClusters){new ArrayBuffer[Int]()}
    for (i <- 0 until clustering.getNNodes) {
      clusters(clustering.getCluster(i)).append(i)
    }
    var writer: BufferedWriter = null
    try {
      writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(output)))
      for (cluster <- clusters) {
        for (node <- cluster) {
          writer.write(node.toString)
          writer.write(" ")
        }
        writer.newLine()
      }
    } finally {
      if (writer != null) {
        writer.close()
      }
    }
  }
}
