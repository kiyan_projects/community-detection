package com.sokian.graph.community

import java.io.{BufferedReader, File, FileReader, IOException}

import cwts.networkanalysis.Network
import cwts.util.{DynamicDoubleArray, DynamicIntArray}

object GraphUtils {
  def readEdgeList(file: File, useModularity: Boolean, weightedEdges: Boolean, sortedEdgeList: Boolean): Network = { // Read edge list.
    val edges = new Array[DynamicIntArray](2)
    val INIT_CAP = 1000000
    val READ_BUFFER_SIZE = 1 << 22
    edges(0) = new DynamicIntArray(INIT_CAP)
    edges(1) = new DynamicIntArray(INIT_CAP)

    val edgeWeights = if (weightedEdges) new DynamicDoubleArray(INIT_CAP) else null
    var nNodes = 0
    var reader: BufferedReader = null
    try {
      reader = new BufferedReader(new FileReader(file), READ_BUFFER_SIZE)
      var line = reader.readLine
      var lineNo = 0
      while (line != null) {
        lineNo += 1
        val columns = line.split(" ")
        if ((!weightedEdges && ((columns.length < 2) || (columns.length > 3))) || (weightedEdges && (columns.length != 3))) {
          throw new IOException("Incorrect number of columns (line " + lineNo + ").")
        }
        var node1 = 0
        var node2 = 0
        try {
          node1 = Integer.parseUnsignedInt(columns(0))
          node2 = Integer.parseUnsignedInt(columns(1))
        } catch {
          case _: NumberFormatException =>
            throw new IOException("Node must be represented by a zero-index integer number (line " + lineNo + ").")
        }
        edges(0).append(node1)
        edges(1).append(node2)
        nNodes = Math.max(nNodes, node1 + 1)
        nNodes = Math.max(nNodes, node2 + 1)
        if (weightedEdges) {
          var weight = .0
          try {
            weight = columns(2).toDouble
          } catch {
            case _: NumberFormatException =>
              throw new IOException("Edge weight must be a number (line " + lineNo + ").")
          }
          edgeWeights.append(weight)
        }
        line = reader.readLine
      }
    } finally {
      if (reader != null) {
        reader.close()
      }
    }

    // Create network.
    val edges2 = new Array[Array[Int]](2)
    edges2(0) = edges(0).toArray
    edges2(1) = edges(1).toArray

    var network: Network = null
    if (weightedEdges) {
      network = new Network(nNodes, useModularity, edges2, edgeWeights.toArray, sortedEdgeList, true)
    } else {
      network = new Network(nNodes, useModularity, edges2, sortedEdgeList, true)
    }
    network
  }
}
