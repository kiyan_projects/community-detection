package com.sokian.graph.community

import cwts.networkanalysis.{Clustering, Network}

object ModularityRegularizationOptimization {

  def calcClusterSizesEdgesDegrees(network: Network, clustering: Clustering): (Array[Long], Array[Long], Array[Long]) = {
    val clusterEdges = new Array[Long](clustering.getNClusters)
    val clusterSizes = new Array[Long](clustering.getNClusters)
    val clusterDegrees = new Array[Long](clustering.getNClusters)

    for (i <- 0 until network.getNNodes) {
      val cluster = clustering.getCluster(i)
      clusterSizes(cluster) += 1
      clusterDegrees(cluster) += network.getNNeighbors(i)
      for (j <- network.getNeighbors(i)) {
        val cluster2 = clustering.getCluster(j)
        if (cluster == cluster2) {
          clusterEdges(cluster) += 1
        }
      }
    }
    for (i <- 0 until clustering.getNClusters) {
      clusterEdges(i) /= 2
    }
    (clusterSizes, clusterEdges, clusterDegrees)
  }

  def calcModularity(network: Network, clustering: Clustering): Double = {
    val (_, clusterEdges, clusterDegrees) = calcClusterSizesEdgesDegrees(network, clustering)
    var quality = 0.0
    for (i <- 0 until clustering.getNClusters) {
      quality += clusterEdges(i)
    }
    quality /= network.getNEdges

    var degreeSum = 0.0
    for (i <- 0 until clustering.getNClusters) {
      val w = clusterDegrees(i) / 2.0 / network.getNEdges.toDouble
      degreeSum += w * w
    }
    quality - degreeSum
  }

  def calcRegularization(network: Network, clustering: Clustering): Double = {
    val (clusterSizes, clusterEdges, _) = calcClusterSizesEdgesDegrees(network, clustering)

    var regularization = 0.0
    for (i <- 0 until clustering.getNClusters) {
      val maxEdges = clusterSizes(i) * (clusterSizes(i) - 1) / 2
      if (clusterSizes(i) == 1) {
        regularization += 1.0
      } else {
        regularization += clusterEdges(i) / maxEdges.toDouble
      }
    }
    regularization /= clustering.getNClusters
    regularization -= clustering.getNClusters / network.getNNodes.toDouble
    regularization *= 0.5
    regularization
  }

  def calcQuality(network: Network, clustering: Clustering): Double = {
    calcModularity(network, clustering) + calcRegularization(network, clustering)
  }
}
