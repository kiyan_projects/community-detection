package com.sokian.graph.community

import cwts.networkanalysis.Network

trait NetworkBuilder {
  def addEdge(from: Int, to: Int): Unit
  def build(): Network
}
