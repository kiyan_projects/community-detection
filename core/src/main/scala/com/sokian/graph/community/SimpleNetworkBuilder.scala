package com.sokian.graph.community

import cwts.networkanalysis.Network

import scala.collection.mutable.ArrayBuffer

class SimpleNetworkBuilder extends NetworkBuilder {
  val edges0 = new ArrayBuffer[Int]()
  val edges1 = new ArrayBuffer[Int]()
  var nNodes = 0

  override def addEdge(from: Int, to: Int): Unit = {
    checkNode(from)
    checkNode(to)
    nNodes = Math.max(nNodes, from + 1)
    nNodes = Math.max(nNodes, to + 1)
    edges0.append(from)
    edges1.append(to)
  }

  override def build(): Network = {
    val edges = new Array[Array[Int]](2)
    edges(0) = edges0.toArray
    edges(1) = edges1.toArray
    new Network(nNodes, true, edges, false, true)
  }

  def resetNNodes(n: Int): Unit = {
    if (n < nNodes) {
      throw new IllegalArgumentException("n should be greater than current nNodes")
    }
    nNodes = n
  }

  private def checkNode(node: Int): Unit = {
    if (node < 0) {
      throw new IllegalArgumentException("Node should be positive integer")
    }
    if (node >= Int.MaxValue - 1) {
      throw new IllegalArgumentException("Node should be less than maxInteger value")
    }
  }
}
