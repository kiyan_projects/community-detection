package com.sokian.graph.community

import cwts.networkanalysis.Clustering
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should
import org.scalatestplus.junit.JUnitRunner


@RunWith(classOf[JUnitRunner])
class ModularityRegularizationOptimizationTest extends AnyFlatSpec with should.Matchers {
  private val EPS = 1e-8

  "Scores" should "calculated correctly for singleton clustering" in {
    val network = TestUtils.createSimpleNetwork()
    val clustering = new Clustering(network.getNNodes)

    var expectedModularity = 0.0
    for (i <- 0 until network.getNNodes) {
      val w = network.getNNeighbors(i).toDouble / 2.0 / network.getNEdges
      expectedModularity -= w * w
    }
    val expectedRegularization = 0.0
    val expectedQuality = expectedModularity + expectedRegularization

    val modularity = ModularityRegularizationOptimization.calcModularity(network, clustering)
    val regularization = ModularityRegularizationOptimization.calcRegularization(network, clustering)
    val quality = ModularityRegularizationOptimization.calcQuality(network, clustering)

    assert(Math.abs(modularity - expectedModularity) < EPS)
    assert(Math.abs(regularization - expectedRegularization) < EPS)
    assert(Math.abs(quality - expectedQuality) < EPS)
  }

  "Scores" should "calculated correctly for one cluster" in {
    val network = TestUtils.createSimpleNetwork()
    val clusters = new Array[Int](network.getNNodes)
    val clustering = new Clustering(clusters)
    val nNodes = network.getNNodes
    val nEdges = network.getNEdges

    val maxEdges = 0.5 * nNodes * (nNodes - 1)

    val expectedModularity = 0.0
    val expectedRegularization = 0.5 * (nEdges / maxEdges - 1.0 / nNodes)
    val expectedQuality = expectedModularity + expectedRegularization

    val modularity = ModularityRegularizationOptimization.calcModularity(network, clustering)
    val regularization = ModularityRegularizationOptimization.calcRegularization(network, clustering)
    val quality = ModularityRegularizationOptimization.calcQuality(network, clustering)

    assert(Math.abs(modularity - expectedModularity) < EPS)
    assert(Math.abs(regularization - expectedRegularization) < EPS)
    assert(Math.abs(quality - expectedQuality) < EPS)
  }

  "Scores" should "calculated correctly for two clusters" in {
    val network = TestUtils.createSimpleNetwork()
    val clusters = Array[Int](0, 0, 0, 0, 1, 1, 1, 1)
    val clustering = new Clustering(clusters)
    val nNodes = network.getNNodes

    def sqr(x: Double): Double = x * x
    def maxEdges(n: Long): Long = n * (n - 1) / 2

    val expectedModularity = 9.0 / 11.0 - sqr(14.0 / 2.0 / 11.0) - sqr(8.0 / 2.0 / 11.0)
    val expectedRegularization = 0.5 * (0.5 * (6.0 / maxEdges(4) + 3.0 / maxEdges(4)) - 2.0 / nNodes)
    val expectedQuality = expectedModularity + expectedRegularization

    val modularity = ModularityRegularizationOptimization.calcModularity(network, clustering)
    val regularization = ModularityRegularizationOptimization.calcRegularization(network, clustering)
    val quality = ModularityRegularizationOptimization.calcQuality(network, clustering)

    assert(Math.abs(modularity - expectedModularity) < EPS)
    assert(Math.abs(regularization - expectedRegularization) < EPS)
    assert(Math.abs(quality - expectedQuality) < EPS)
  }
}
