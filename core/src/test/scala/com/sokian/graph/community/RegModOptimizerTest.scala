package com.sokian.graph.community

import cwts.networkanalysis.{Clustering, Network}
import org.junit.runner.RunWith
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatestplus.junit.JUnitRunner

@RunWith(classOf[JUnitRunner])
class RegModOptimizerTest extends AnyFlatSpec {
  private val EPS = 1e-8

  def calcDeltaScore(network: Network, clusteringInitial: Clustering, clusteringFinal: Clustering): Double = {
    val initialScore = ModularityRegularizationOptimization.calcQuality(network, clusteringInitial)
    val finalScore = ModularityRegularizationOptimization.calcQuality(network, clusteringFinal)
    finalScore - initialScore
  }

  "RegModOptimizer" should "correctly calculate delta scores 1 node graph" in {
    val networkBuilder = new SimpleNetworkBuilder
    networkBuilder.resetNNodes(1)
    val network = networkBuilder.build()

    val clustering = new Clustering(1)

    val regModOptimizer = new RegModOptimizer(network, clustering)
    val expectedDelta = 0.0
    val deltaScore = regModOptimizer.getDeltaScore(0)

    assert(Math.abs(deltaScore - expectedDelta) < EPS)
  }

  "RegModOptimizer" should "correctly calculate delta scores 2 node graph" in {
    val networkBuilder = new SimpleNetworkBuilder
    networkBuilder.addEdge(0, 1)
    val network = networkBuilder.build()
    val clusters = Array[Int](0, 0)

    val clusteringInitial = new Clustering(clusters)
    val clusteringFinal = new Clustering(2)

    val regModOptimizer = new RegModOptimizer(network, clusteringInitial)
    val expectedDelta = calcDeltaScore(network, clusteringInitial, clusteringFinal)
    val deltaScore1 = regModOptimizer.getDeltaScore(0)
    val deltaScore2 = regModOptimizer.getDeltaScore(1)

    assert(Math.abs(deltaScore1 - expectedDelta) < EPS)
    assert(Math.abs(deltaScore2 - expectedDelta) < EPS)
  }

  "RegModOptimizer" should "correctly calculate delta scores 3 node graph 1" in {
    val networkBuilder = new SimpleNetworkBuilder
    networkBuilder.addEdge(0, 1)
    networkBuilder.addEdge(0, 2)
    networkBuilder.addEdge(1, 2)

    val network = networkBuilder.build()
    val clusters = Array[Int](0, 0, 0)

    val clusteringInitial = new Clustering(clusters)

    val regModOptimizer = new RegModOptimizer(network, clusteringInitial)
    val deltaScore1 = regModOptimizer.getDeltaScore(0)
    val deltaScore2 = regModOptimizer.getDeltaScore(1)
    val deltaScore3 = regModOptimizer.getDeltaScore(2)

    assert(Math.abs(deltaScore1 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 1, 1)))) < EPS)
    assert(Math.abs(deltaScore2 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 1, 0)))) < EPS)
    assert(Math.abs(deltaScore3 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 0, 1)))) < EPS)
  }

  "RegModOptimizer" should "correctly calculate delta scores 3 node graph 2" in {
    val networkBuilder = new SimpleNetworkBuilder
    networkBuilder.addEdge(0, 1)
    networkBuilder.addEdge(0, 2)

    val network = networkBuilder.build()
    val clusters = Array[Int](0, 0, 0)

    val clusteringInitial = new Clustering(clusters)

    val regModOptimizer = new RegModOptimizer(network, clusteringInitial)
    val deltaScore1 = regModOptimizer.getDeltaScore(0)
    val deltaScore2 = regModOptimizer.getDeltaScore(1)
    val deltaScore3 = regModOptimizer.getDeltaScore(2)

    assert(Math.abs(deltaScore1 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 1, 1)))) < EPS)
    assert(Math.abs(deltaScore2 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 1, 0)))) < EPS)
    assert(Math.abs(deltaScore3 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 0, 1)))) < EPS)
  }

  "RegModOptimizer" should "correctly calculate delta scores 3 node graph 3" in {
    val networkBuilder = new SimpleNetworkBuilder
    networkBuilder.addEdge(0, 1)
    networkBuilder.resetNNodes(3)

    val network = networkBuilder.build()
    val clusters = Array[Int](0, 0, 1)

    val clusteringInitial = new Clustering(clusters)

    val regModOptimizer = new RegModOptimizer(network, clusteringInitial)
    val deltaScore1 = regModOptimizer.getDeltaScore(0)
    val deltaScore2 = regModOptimizer.getDeltaScore(1)
    val deltaScore3 = regModOptimizer.getDeltaScore(2)

    assert(Math.abs(deltaScore1 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 1, 2)))) < EPS)
    assert(Math.abs(deltaScore2 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 1, 2)))) < EPS)
    assert(Math.abs(deltaScore3 - calcDeltaScore(network, clusteringInitial, new Clustering(Array[Int](0, 0, 1)))) < EPS)
  }

  "RegModOptimizer" should "correctly calculate delta scores for standard graph and two clusters" in {
    val network = TestUtils.createSimpleNetwork()
    val clusters = Array[Int](0, 0, 0, 0, 1, 1, 1, 1)

    val clusteringInitial = new Clustering(clusters)

    val regModOptimizer = new RegModOptimizer(network, clusteringInitial)
    val deltaScores = new Array[Double](network.getNNodes)
    for (i <- 0 until network.getNNodes) {
      deltaScores(i) = regModOptimizer.getDeltaScore(i)
    }

    for (i <- 0 until network.getNNodes) {
      val newClusters = clusters.clone()
      newClusters(i) = 2
      val clustering = new Clustering(newClusters)
      val expected = calcDeltaScore(network, clusteringInitial, clustering)
      assert(Math.abs(deltaScores(i) - expected) < EPS)
    }
  }

  "RegModOptimizer" should "correctly calculate delta scores after manipulation for standard graph and two clusters" in {
    val network = TestUtils.createSimpleNetwork()

    val clustersArray = Array[Array[Int]](
      Array[Int](0, 0, 0, 0, 1, 1, 1, 1),
      Array[Int](2, 0, 0, 0, 1, 1, 1, 1),
      Array[Int](2, 0, 3, 0, 1, 1, 1, 1),
      Array[Int](2, 0, 3, 0, 4, 1, 1, 1),
      Array[Int](2, 0, 3, 0, 4, 1, 5, 1)
    )
    val movedNodes = Array[Int](0, 2, 4, 6)

    val regModOptimizer = new RegModOptimizer(network, new Clustering(clustersArray(0)))

    for (i <- movedNodes.indices) {
      val clusteringInitial = new Clustering(clustersArray(i))
      val clusteringFinal = new Clustering(clustersArray(i + 1))
      val expected = calcDeltaScore(network, clusteringInitial, clusteringFinal)

      val score = regModOptimizer.getDeltaScore(movedNodes(i))
      assert(Math.abs(score - expected) < EPS)
      regModOptimizer.moveToNewCluster(movedNodes(i))
    }
  }
}
