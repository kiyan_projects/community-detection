package com.sokian.graph.community

import cwts.networkanalysis.Network

object TestUtils {
  def createSimpleNetwork(): Network = {
    val networkBuilder = new SimpleNetworkBuilder
    networkBuilder.addEdge(0, 1)
    networkBuilder.addEdge(0, 2)
    networkBuilder.addEdge(0, 3)

    networkBuilder.addEdge(1, 2)
    networkBuilder.addEdge(1, 3)

    networkBuilder.addEdge(2, 3)

    networkBuilder.addEdge(3, 4)
    networkBuilder.addEdge(3, 5)

    networkBuilder.addEdge(4, 5)

    networkBuilder.addEdge(5, 6)
    networkBuilder.addEdge(5, 7)

    networkBuilder.build()
  }
}
